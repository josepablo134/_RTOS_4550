#include "hardware.h"

void Hardware_init(void) {
    //
    //  Inicializar puerto A
    //
    PORTA = 0;
    LATA = 0b00000000;
    TRISA = 0b00000100;
    CMCON = 0x07;
    
    //
    //  Inicializar puerto B
    //
    PORTB   = LATB = 0x00;
    TRISB   = 0x03;//INT2 - 0 son entradas
    
    //
    //  Configurar bits de interrupcion.
    //  El Scheduler se encarga de activar
    //  las interrupciones y la prioridad.
    //
    
    //Esta interrupcion siempre es de alta prioridad
/*    INTCONbits.INT0IE  =    1;//Activar interrupcion 0
    INTCONbits.INT0IF  =    0;
    INTCON2bits.INTEDG0 =   0;//falling edge
    
    INTCON2bits.RBPU    =   0;//�Weak Pull-up!
    LATBbits.LATB0      =   1;//Bit-0 tiene pull-up
    LATBbits.LATB1      =   1;//Bit-1 tiene pull-up
    
    INTCON3bits.INT1IF  =  0;
    INTCON3bits.INT1IE  =  1;//Activar interrupcion 1
    INTCON3bits.INT1IP  =  0;//Interrupcion de baja prioridad
    INTCON2bits.INTEDG1 =  1;//rising edge
 */
    
    ADCON1 = 0x0F;
    
    //UART_init();
    IIC_init();
    CCP_init();
    ADC_init();
}

/**
 *  Configuracion del modulo IIC
 */
void IIC_init(void){
    /* DUMMY FUNCTION */
}
/**
 *  Configuracion del modulo PWM
 */
void CCP_init(void){
    /* ENCENDER SE�AL PWM 
     * Set the PWM period by writing to the PR2
     * register.
     * 
     * Set the PWM duty cycle by writing to the
     * CCPRxL register and CCPxCON<5:4> bits.
     * 
     * Make the CCPx pin an output by clearing the
     * appropriate TRIS bit.
     * 
     * Set the TMR2 prescale value, then enable
     * Timer2 by writing to T2CON.
     * 
     * Configure the CCPx module for PWM operation.
     * 
     * Se configura para 46875 Hz
     */
    PR2 = 0xFF;//Period Register
    
    CCPR1L              = 0;
    CCPR1H              = 0;
    CCP1CON = 0;
    
    CCPR2L              = 0;//MSB  0% Duty Cycle
    CCPR2H              = 0;//
    CCP2CON = 0;//    CCP2CONbits.DC2B    = 0;//LSB
    
    PORTC = LATC = 0;
    TRISC = 0;//Prepare CCP1
    
    T2CON  = 0;//Prescale = 1, Postscale = 1, Tmr2 = Off
    TMR2   = 0;
    T2CONbits.TMR2ON = 1;
    
    CCP1CONbits.CCP1M    = 0b00001100;//PWM Mode
    CCP2CONbits.CCP2M    = 0b00001100;
    //Alternate pin assignment for CCP2 when CCP2MX = 0. Default assignment is RC1.
}
/**
 *  Configuracion del modulo ADC
 */
void ADC_init(void){
    /**
     * A/D Result High Register (ADRESH)
     * A/D Result Low Register (ADRESL)
     * A/D Control Register 0 (ADCON0)
     * A/D Control Register 1 (ADCON1)
     * A/D Control Register 2 (ADCON2)
     */
    TRISA |= 0b00000011;
    ADCON0 = 0x00;//Apagar ADC
    ADCON1 = 0b00001101;// Referencia -> Alimentacion ; A0 y A1 Activados como ADC
    ADCON2 = 0b10100100;// AD Right Justified ; 8 TAD  ; Conversion Clock Fosc / 4 ;
    ADCON0 = 0x01;//ADC Encendido ; Pero no trabajando
}