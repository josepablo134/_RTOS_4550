#ifndef _HARDWARE_H
#define	_HARDWARE_H
    #include "RTOS/RTOS4550.h"

    /**
    *   Configuracion del hardware global 
    */
    extern void Hardware_init(void);
    /**
     *  Configuracion del modulo IIC
     */
    extern void IIC_init(void);
    /**
     *  Configuracion del modulo PWM
     */
    #define PWM1_Duty10b(DUTY){CCPR1 = DUTY>>2;CCP1CONbits.DC1B = DUTY & 0x03;}
    #define PWM2_Duty10b(DUTY){CCPR2 = DUTY>>2;CCP2CONbits.DC2B = DUTY & 0x03;}
    extern void CCP_init(void);
    /**
     *  Configuracion del modulo ADC
     */
    #define AD_A0_GO()    { ADCON0 = 0b00000011; }
    #define AD_A1_GO()    { ADCON0 = 0b00000111; }
    extern void ADC_init(void);
#endif

