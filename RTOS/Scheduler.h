#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_
    #include "task.h"
    #include "pic_configuration.h"


    /** @name   Scheduler_Macros
     *      Macros para controlar el sistema operativo.
     * 
     */
    //
    //  Evitar que el Scheduler haga un cambio de contexto
    //
    #define Scheduler_stop   { T0CONbits.TMR0ON = 0;INTCONbits.GIE = 0; }
    
    //
    //  Permitir al Scheduler realizar cambios de contexto
    //
    #define Scheduler_start  { T0CONbits.TMR0ON = 1;INTCONbits.GIE = 1; }

    //
    //  Dar tiempo a siguiente tarea.
    //
    #define Scheduler_pass   {T0CONbits.TMR0ON = 0;TMR0 = 0xFFFF;T0CONbits.TMR0ON = 1;}

    //
    //  Asignar mas tiempo a la tarea actual.
    //
    #define Scheduler_tmr(DATA) {TMR0 = DATA;}


    //
    //  Numero maximo de tareas que el Scheduler administrara
    //
    #ifndef MAX_TASK
        #define MAX_TASK    5
    #endif


/** @name   Scheduler
 *      La siguiente seccion define las funciones que el Scheduler utilizara
 *  sobre ejecucion y no se recomienda modificar.
 */
    
    //
    //  Registro de callbacks que el procesador ejecutara como interrupciones.
    //  Estos registros deben ser modificados a traves de las
    //  funciones correspondientes.
    //
    task_function       USER_ISR_HIGH=0x00;
    task_function       USER_ISR_LOW=0x00;
    
    /**
     * Permite registrar una tarea en la lista de tareas del calendarizador.
     * @param Task*             Apuntador al registro handle de la tarea.
     * @param Task_stack        Pila de la tarea.
     * @param task_function     callback, funcion que se ha de ejecutar
     *                          como tarea.
     */
    void TaskRegister(Task*,Task_stack,task_function);
    
    /**
     * Permite registrar un callback como
     * funcion de atencion a la interrupcion.
     * @param 
     */
    #define RegisterHigh_ISR(callback)  USER_ISR_HIGH = callback
    /**
     * Permite registrar un callback como funcion de atencion
     * a la interrupcion.
     * @param 
     */
    #define RegisterLow_ISR(callback)   USER_ISR_LOW = callback
    
    /**
     * Inicializar el sistema.
     */
    void StartRTOS(void);
    
    /**
     * Calendarizador, funcion que controla los
     * cambios de control del CPU.
     */
    void Scheduler(void);
    /**
     * Funcion osciosa, cuando se haya caido en un caso controlado de excepcion,
     * la ejecucion llevara a esta funcion.
     */
    void Exception(void);
    
    /**
     * Funcion de interrupcion de alta prioridad.
     * Atendido por el Scheduler.
     */
//    void interrupt HISR(void);
    /**
     * Funcion de interrupcion de baja prioridad.
     * Atendido por el Scheduler.
     */
//    void interrupt low_priority LISR(void);
#endif