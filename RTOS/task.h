#ifndef _TASK_H_
#define _TASK_H_
    #include <stdint.h>
    #define TASK_READY      0b00000001
    #define TASK_RUNNING    0b00000010
    #define TASK_BLOCKED    0b00000100
    #define TASK_DONE       0b00000000
    
    typedef void(*task_function)(void);
    typedef uint32_t  Task_stack_element;
    typedef uint32_t* Task_stack;
    
    typedef struct Context{
        unsigned char WREG;
        unsigned char BSR;
        unsigned char STATUS;
        uint16_t FSR_0,FSR_1,FSR_2;
        uint32_t PC;
        uint32_t *STACK;
        unsigned char STKPTR;
    }Context;
    
    typedef struct Task{
        Context         context_reg;
        unsigned char   State;
        task_function   function;
    }Task;
    
    void Task_init(Task*,Task_stack,task_function);

#endif