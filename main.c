#include "RTOS/RTOS4550.h"
#include "hardware.h"
#include "UART/UART.h"

char str[] = "Hello world!  \nThis is a RTOS Running on PIC18F4550!\n";

Task_stack_element  Tarea0_Pila[31];
Task                Tarea0_Handle;
Task_stack_element  Tarea1_Pila[31];
Task                Tarea1_Handle;
Task_stack_element  Tarea2_Pila[31];
Task                Tarea2_Handle;
Task_stack_element  Tarea3_Pila[31];
Task                Tarea3_Handle;
Task_stack_element  Tarea4_Pila[31];
Task                Tarea4_Handle;

void Task0(void);
void Task1(void);
void Task2(void);
void Task3(void);
void Task4(void);

void HighISR(void);
void LowISR(void);

unsigned char sem=0;

void main(void) {
    //
    //  Inicializamos hardware necesario
    //
    Hardware_init();//PUERTO A: BIT 0 y 2 como salida. BIT 1 como entrada.
    UART_init();
    UART_open(115200);
    
    //
    //  Se registra la tarea 0
    //
    TaskRegister(&Tarea0_Handle,Tarea0_Pila,Task0);
    //
    //  Se registra la tarea 1
    //
    TaskRegister(&Tarea1_Handle,Tarea1_Pila,Task1);
    
    //
    //  Se registra la tarea 2
    //
    TaskRegister(&Tarea2_Handle,Tarea2_Pila,Task2);
    //
    //  Se registra la tarea 3
    //
    TaskRegister(&Tarea3_Handle,Tarea3_Pila,Task3);
    //
    //  Se registra la tarea 3
    //
    TaskRegister(&Tarea4_Handle,Tarea4_Pila,Task4);
    
    //
    //  Se registra la interrupcion de baja prioridad
    //
    RegisterLow_ISR( LowISR );
    //
    //  Se registra la interrupcion de alta prioridad
    //
    RegisterHigh_ISR( HighISR );
    //
    //Comenzar a ejecutar las tareas!
    //  El sistema siempre comenzara a ejecutar
    //  la primer tarea registrada en lista.
    //
    StartRTOS();
    while(1);//Esta linea jamas deberia ser alcanzada pues la pila es
             //vaciada durante los procesos del Scheduler.
}

unsigned short PWM1_OUT = 0;
void Task0(){
    while(1){
		if(sem){
			/// Si la salida esta lista
			PWM1_Duty10b(PWM1_OUT);
			sem = 0;
		}
        Scheduler_pass;
    }
}
void Task1(){
    while(1){
			AD_A0_GO();
			while( ADCON0bits.DONE ){ Scheduler_pass; }/// Esperar por el ADC
            PWM1_OUT = (ADRESH<<8) | (ADRESL);
			sem = 1;
    }
}
void Task2(){
    unsigned long counter;
    unsigned char var1=0;
    while(1){
        counter++;
        if(counter >= 1500){
            var1 = (var1+1)%10;
            str[13] = '0' + var1;
            Scheduler_stop;
            UART_Transmit( str , sizeof(str) );
            Scheduler_start;
            while( UART_Status() & TX_BUSY ){ Scheduler_pass; }
            counter = 0;
        }
        Scheduler_pass;
    }
}
void Task3(){
    while(1){
        LATAbits.LA4 = !PORTAbits.RA2;
        Scheduler_pass;
    }
}
void Task4(){
    while(1){
        LATAbits.LA3 = PORTAbits.RA2;
        Scheduler_pass;
    }
}
void HighISR(void){
    asm("NOP");
    //UART_INTERRUPT;
}
void LowISR(void){
    UART_INTERRUPT;
}


//
//  @FIN }
//